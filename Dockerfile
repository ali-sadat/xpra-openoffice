FROM ubuntu:16.04
MAINTAINER Ali Sadat <sadatakhavi.ali@gmail.com>

# Set environment variables.
ENV APP_NAME="Firefox"
ENV LANG en-US
ENV DEBIAN_FRONTEND noninteractive

COPY local.conf /etc/fonts/local.conf

# Update the system
RUN apt-get update \
  && apt-get install -y \
  apt-utils \
  wget \
                       libx11-dev libxtst-dev libxcomposite-dev \
                       libxdamage-dev libxkbfile-dev python-all-dev \
                       python-gobject-dev python-gtk2-dev cython \
                       xvfb xauth x11-xkb-utils libx264-dev libvpx-dev \
                       libswscale-dev libavcodec-dev subversion websockify curl \
  libcanberra-gtk-module \ 
  libcanberra-gtk3-module \
  dbus-x11 ttf-ubuntu-font-family \
  --no-install-recommends \
  && rm -rf /var/lib/apt/lists/*

#==============
# Install Xpra
#==============
RUN apt-get update \
  && apt-get install -y \
  xpra \
  xserver-xorg-video-dummy 

ADD http://xpra.org/xorg.conf /home/xpra/xorg.conf
RUN /bin/echo -e "export DISPLAY=:100" > /home/xpra/.profile        # && chown xpra:xpra /home/xpra/xorg.conf

#==============
# Install OpenOffice
#==============
RUN apt-get install -y software-properties-common \
  && add-apt-repository ppa:openjdk-r/ppa \
  && apt-get update \
  && apt-get install -y \
  openjdk-7-jdk \
  --no-install-recommends \
  && rm -rf /var/lib/apt/lists/*
 
 RUN apt-get update \
  && apt-get install -y \
  ca-certificates \
  && wget http://sourceforge.net/projects/openofficeorg.mirror/files/4.0.0/binaries/en-US/Apache_OpenOffice_4.0.0_Linux_x86-64_install-deb_en-US.tar.gz \
  && tar -xvf Apache_OpenOffice*.tar.gz \
  && dpkg -i en-US/DEBS/*.deb \
  && dpkg -i en-US/DEBS/desktop-integration/*.deb \
  && rm Apache_OpenOffice_4.0.0_Linux_x86-64_install-deb_en-US.tar.gz

#COPY configFiles/registrymodifications.xcu /home/openoffice/.openoffice/5/user/registrymodifications.xcu

# expose raw TCP port
EXPOSE 10000

# Run! Application will be served both as WebSocket and Raw TCP. (+ disable all unsupported bits)
# https://wiki.openoffice.org/wiki/Documentation/OOoAuthors_User_Manual/Getting_Started/Starting_from_the_command_line
# -headless -nofirststartwizard
CMD ["xpra", "start", ":100", "--start-child=soffice -writer -nologo -norestore",  "--daemon=off", "--bind-tcp=0.0.0.0:10000", "--no-mdns", "--no-notifications", "--no-pulseaudio"]


